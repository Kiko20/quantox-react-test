Project name: Kristijan's Quantox frontend developer test
Project description: Weather app
Time to build; 8 hours

In this project I used React as a main framework, JavaScript , HTML5 and CSS.
NPM packages used in the project: Styled-components, hookrouter.

NOTE == Before you run "NPM START" please run "NPM INSTALL" to install the neccesery packages.

This project is single page mobile friendly application where I use the hookrouter to manupulate with the url and switch between 2 components. On the main page is Homepage.js component and for entering wrong url there is an Error page or Error.js.
On the Homepage we have a list of cards generated from the WeatherData contained in the WeatherData.js. On the cards we have the day and the temperature for each day in the week. By clicking on any of the card, we flip we front page, and we see the other information for that day (windSpeed, windDirection and Type). I used the rotate method in CSS to flip and animate the cards. At the top of the page we have the Celsius and M/S buttons. When we click on the Celsius button we toggle the values on the temperature from Celisus to Kelvin, where if we click on the M/S button we toggle from M/S to Km/H. For the time given I used all the latest technologies and I think that I can improve the application even more if there was more time. I switched from "Widget" to Card Flipping because I thought that it is more user friendly.
