import React from "react";
import Homepage from "./HomePage";

const Routes = {
  "/": () => <Homepage />,
};

export default Routes;
