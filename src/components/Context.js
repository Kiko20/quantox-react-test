import React, { createContext, useEffect, useState } from "react";
import weatherData from "./WeatherData";

export const Context = createContext({});

export const Provider = (props) => {
  const [tempUnit, setTempUnit] = useState("C");
  const [windSpeedUnit, setWindSpeedUnit] = useState("m/h");
  const [data, setData] = useState([]);
  //Use effect for setting the data in the state
  useEffect(() => {
    setData(weatherData);
  }, []);
  //Function for converting Celsius to Kelvin
  const CtoKelvin = () => {
    setData(
      data.map((el) =>
        tempUnit == "C"
          ? { ...el, temp: Math.round(el.temp + 273.15) }
          : { ...el, temp: Math.round(el.temp - 273.15) }
      )
    );
  };
  //Function for converting Miles to Km
  const MilestoKm = () => {
    setData(
      data.map((el) =>
        windSpeedUnit == "m/h"
          ? { ...el, windSpeed: Math.round(el.windSpeed * 1.609) }
          : { ...el, windSpeed: Math.round(el.windSpeed / 1.609) }
      )
    );
  };

  return (
    <Context.Provider
      value={{
        tempUnit,
        windSpeedUnit,
        data,
        setTempUnit,
        setWindSpeedUnit,
        CtoKelvin,
        MilestoKm,
      }}
    >
      {props.children}
    </Context.Provider>
  );
};
