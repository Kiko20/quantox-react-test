import React, { useContext } from "react";
//NPM PACKAGE
import styled from "styled-components";
//COMPONENTS
import { Context } from "./Context";
import Card from "./Card";
//IMAGES
import Background from "../Images/weather.jpg";

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
`;
const WeatherCardWrapper = styled.div`
  width: 80%;
  display: flex;
  flex-wrap: wrap;
  @media (min-width: 425px) and (max-width: 900px) {
    justify-content: center;
  }
`;
const BtnWrapper = styled.div`
  display: flex;
  button {
    background-color: white;
    border: none;
    padding: 5px 15px;
    margin: 5px;
    width: 100px;
    border-radius: 3px;
    &:focus {
      outline: none;
    }
    text-transform: capitalize;
  }
`;
const HomePage = () => {
  const {
    data,
    setTempUnit,
    tempUnit,
    setWindSpeedUnit,
    windSpeedUnit,
    CtoKelvin,
    MilestoKm,
  } = useContext(Context);
  return (
    <Wrapper bg={Background}>
      <BtnWrapper>
        <button
          onClick={() =>
            tempUnit === "C"
              ? (setTempUnit("kelvin"), CtoKelvin())
              : (setTempUnit("C"), CtoKelvin())
          }
        >
          {tempUnit}
        </button>
        <button
          onClick={() =>
            windSpeedUnit === "m/h"
              ? (setWindSpeedUnit("km/h"), MilestoKm())
              : (setWindSpeedUnit("m/h"), MilestoKm())
          }
        >
          {windSpeedUnit}
        </button>
      </BtnWrapper>
      <WeatherCardWrapper>
        {data.map((el, i) => {
          return (
            <Card
              key={i}
              day={el.day}
              temp={el.temp}
              windDirection={el.windDirection}
              windSpeed={el.windSpeed}
              type={el.type}
            />
          );
        })}
      </WeatherCardWrapper>
    </Wrapper>
  );
};

export default HomePage;
