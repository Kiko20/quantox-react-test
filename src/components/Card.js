import React, { useContext, useState } from "react";
//NPM PACKAGES
import styled from "styled-components";
//Components
import { Context } from "./Context";

const Wrapper = styled.div`
  margin: 30px;
  padding: 30px;
  width: 250px;
  height: 150px;
  background-color: rgba(255, 255, 255, 0.5);
  cursor: pointer;
  @media (max-width: 425px) {
    padding: 16px;
  }
  @media (min-width: 768px) and (max-width: 1000px) {
    margin: 20px;
  }
`;
const Inner = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
  text-align: center;
  transition: 0.6s;
  transform-style: preserve-3d;
  /* box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2); */
  /* ${Wrapper}:hover & {
    transform: rotateY(180deg); ///Option to flip the cards on hover
  } */
  transform: ${(props) => (props.rotate ? "rotateY(180deg)" : "rotateY(0deg)")};
`;
const FrontSide = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  backface-visibility: hidden;
`;
const BackSide = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  backface-visibility: hidden;
  transform: rotateY(180deg);
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: space-between;
`;
const Day = styled.h1`
  font-size: 35px;
  margin: 0;
  @media (max-width: 425px) {
    font-size: 25px;
  }
`;
const Temp = styled.p`
  font-size: 45px;
  margin: 0;
  @media (max-width: 425px) {
    font-size: 35px;
  }
`;
const WindDirection = styled.p`
  align-self: center;
  font-size: 50px;
  position: static;
  top: 0;
  margin: 0;
  padding: 0;
  line-height: 1;
  transform: ${(props) =>
    props.direction == "north-east"
      ? "rotate(225deg)"
      : props.direction === "north-west"
      ? "rotate(135deg)"
      : props.direction === "south-east"
      ? "rotate(-45deg)"
      : props.direction === "south-west"
      ? "rotate(45deg)"
      : props.direction === "north"
      ? "rotate(180deg)"
      : props.direction === "west"
      ? "rotate(90deg)"
      : props.direction === "east"
      ? "rotate(-90deg)"
      : "rotate(0deg)"};
`;
const Info = styled.p`
  margin: 0;
  padding: 0;
  font-size: 18px;
  line-height: 1;
`;
const Card = (props) => {
  const { tempUnit, windSpeedUnit } = useContext(Context);
  const [rotated, setRotated] = useState(false); //toggle for the flipping cards
  return (
    <>
      <Wrapper onClick={() => setRotated(!rotated)}>
        <Inner rotate={rotated}>
          <FrontSide>
            <Day>{props.day}</Day>
            <Temp>
              {props.temp}
              {tempUnit}
            </Temp>
          </FrontSide>
          <BackSide>
            <WindDirection direction={props.windDirection}>
              &#8593;
            </WindDirection>

            <Info>Wind Direction: {props.windDirection}</Info>
            <Info>
              Wind Speed:{props.windSpeed}
              {windSpeedUnit}
            </Info>
            <Info>Type: {props.type}</Info>
          </BackSide>
        </Inner>
      </Wrapper>
    </>
  );
};

export default Card;
