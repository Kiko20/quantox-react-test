import React from "react";
import styled from "styled-components";

const ErrorDiv = styled.div`
  font-size: 60px;
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Error = () => {
  return <ErrorDiv>404 Not Found</ErrorDiv>;
};

export default Error;
