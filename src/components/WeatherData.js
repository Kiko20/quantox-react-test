const weatherData = [
  {
    day: "Monday",
    temp: 22,
    windDirection: "north-east",
    windSpeed: "10",
    type: "sunny",
  },
  {
    day: "Tuesday",
    temp: 25,
    windDirection: "north-west",
    windSpeed: "13",
    type: "sunny",
  },
  {
    day: "Wednesday",
    temp: 18,
    windDirection: "west",
    windSpeed: "3",
    type: "sunny",
  },
  {
    day: "Thursday",
    temp: 16,
    windDirection: "south-east",
    windSpeed: "6",
    type: "cloudy",
  },
  {
    day: "Friday",
    temp: 12,
    windDirection: "east",
    windSpeed: "22",
    type: "rainy",
  },
  {
    day: "Saturday",
    temp: 10,
    windDirection: "south-west",
    windSpeed: "25",
    type: "rainy",
  },
  {
    day: "Sunday",
    temp: 24,
    windDirection: "south",
    windSpeed: "1",
    type: "sunny",
  },
];

export default weatherData;
